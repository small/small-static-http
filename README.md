# small-static-http

small-static-http is a small http server written in php using openswoole to serve static files.

You can use docker image on Docker Hub [https://hub.docker.com/r/sebk69/small-static-http](https://hub.docker.com/r/sebk69/small-static-http) :

```bash
$ docker pull sebk69/small-static-http
```

## build args

- memory_limit : Max allowed memory size
- timezone : timezone for logs
- user_id : user id for www-data user

## volumes

- /var/www : directory to serve
- /etc/small-static-http.json : configuration file

## configuration file
```
{
  "root-path": "/var/www",
  "index-files": ["index.html"],
  "not-found": {
    "page": "/not-found.html",
    "status": 404
  },
  "skip-hidden-files": true,
  "http": {
    "port": 80,
    "swoole": {
      "worker_num": 8,
      "backlog": 128
    }
  }
}
```

## Docker-compose example

```
version: "3.7"

services:
   static-server:
       image: sebk69/small-static-http:latest
       ports:
          - 80:80
       volumes
          - ./config.json:/etc/small-static-http.json
          - /home/user/my-site:/var/www
```