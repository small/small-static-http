FROM openswoole/php:8.2

# args
ARG memory_limit=500M
ARG timezone=Europe/Paris
ARG user_id=1000

RUN pecl install openswoole && docker-php-ext-enable openswoole

# install zip php extension
RUN apt-get update && apt-get install -y zip libzip-dev
RUN docker-php-ext-install zip

# set memory limit
RUN echo "memory_limit = $memory_limit" >> /usr/local/etc/php/conf.d/docker-php-ram-limit.ini

# Set timezone
RUN cp /usr/share/zoneinfo/$timezone /etc/localtime \
    && echo "$timezone" > /etc/timezone \
    && echo "[Date]\ndate.timezone=$timezone" > /usr/local/etc/php/conf.d/timezone.ini

# default config
COPY ./default-config.json /etc/small-static-http.json

# install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php --install-dir=/bin --filename=composer
RUN php -r "unlink('composer-setup.php');"

# setup directories
RUN mkdir /usr/src/app
WORKDIR /usr/src/app

# copy sources files
COPY ./src /usr/src/app

# install dependencies
RUN apt-get update && apt-get install -y git
RUN composer update

# expose http port
EXPOSE 80

# common packages
RUN apt-get update && apt-get install procps htop wget

# setup user
RUN usermod -u 1000 www-data
RUN chown -R 1000:1000 /var/www
USER www-data

# entrypoint
EXPOSE 80
ENTRYPOINT /usr/src/app/bin/serve
