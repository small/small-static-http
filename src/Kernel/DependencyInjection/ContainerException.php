<?php

/*
 * This file is a part of small-static-http
 * Copyright 2022-2023- Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace SmallStaticHttp\Kernel\DependencyInjection;

use Psr\Container\ContainerExceptionInterface;

class ContainerException extends \Exception implements ContainerExceptionInterface
{

}