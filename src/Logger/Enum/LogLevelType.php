<?php

/*
 * This file is a part of small-static-http
 * Copyright 2022-2023- Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace SmallStaticHttp\Logger\Enum;
enum LogLevelType
{

    case info;
    case error;
    case critical;

}