<?php

/**
 * This file is a part of small-static-http
 * Copyright 2022-2023 - Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace SmallStaticHttp\Logger;

use Small\Logger\Contracts\FormatterInterface;
use Small\Logger\Contracts\LogInterface;
use Small\Logger\Contracts\OutputInterface;
use Small\Logger\Contracts\StreamInterface;
use Small\Logger\Contracts\SwitchLogicInterface;
use Small\Logger\Formatter\JsonFormatter;
use Small\Logger\Output\Config\FileOutputConfig;
use Small\Logger\Output\Config\HttpConfig;
use Small\Logger\Output\Config\StdOutputConfig;
use Small\Logger\Output\FileOutput;
use Small\Logger\Output\StdOutput;
use Small\Logger\Output\SwooleHttpOutput;
use Small\Logger\Stream\Stream;
use SmallStaticHttp\Kernel\Kernel;
use SmallStaticHttp\Logger\Enum\FormatType;
use SmallStaticHttp\Logger\Formatter\TextFormatter;
use SmallStaticHttp\Logger\Log\KernelLog;
use SmallStaticHttp\Logger\Log\HttpAccessLog;

class SwitchLogic implements SwitchLogicInterface
{

    protected Stream $streamKernel;
    protected Stream $streamAccess;
    protected Stream $streamError;

    public function __construct()
    {
        $config  = Kernel::$container->getParameter('logs');

        foreach ($config as $kindConfig) {
            switch ($kindConfig['kind']) {
                case 'kernel':
                    $this->streamKernel = $this->createStream($kindConfig);
                    break;
                case 'access':
                    $this->streamAccess = $this->createStream($kindConfig);
                    break;
                case 'error':
                    $this->streamError = $this->createStream($kindConfig);
                    break;
                default:
                    throw new \LogicException('Unknown log kind (' . $kindConfig['kind'] . ')');
            }
        }

    }

    /**
     * Create stream from config
     * @param array $config
     * @return Stream
     */
    protected function createStream(array $config): Stream
    {
        switch ($config['type']) {
            case 'stdout':
                return new Stream(
                    $this->createFormatter(FormatType::from($config['format'])),
                    (new StdOutput())
                        ->setConfig(new StdOutputConfig(STDOUT))
                );
            case 'file':
                return new Stream(
                    $this->createFormatter(FormatType::from($config['format'])),
                    (new FileOutput())
                        ->setConfig(new FileOutputConfig($config['path']))
                );
            case 'http':
                return new Stream(
                    $this->createFormatter(FormatType::from($config['format'])),
                    (new SwooleHttpOutput())
                        ->setConfig(
                            (new HttpConfig($config['host'], $config['port'], $config['protocol'] == 'https'))
                                ->addHeader('Content-Type', 'application/json')
                        )

                );
        }
    }

    /**
     * Create formatter from config
     * @param string $format
     * @return OutputInterface
     */
    protected function createFormatter(FormatType $format): FormatterInterface
    {
        return match ($format) {
            FormatType::text => new TextFormatter(),
            FormatType::json => new JsonFormatter(),
        };
    }

    /**
     * Get Stream from log
     * @param LogInterface $log
     * @param array $data
     * @return StreamInterface
     */
    public function getStream(LogInterface $log, array $data = []): StreamInterface
    {

        if ($log instanceof KernelLog) {
            return $this->streamKernel;
        }

        if ($log instanceof HttpAccessLog) {
            return $this->streamAccess;
        }

        if ($log->getLevel() == 'error') {
            return $this->streamError;
        }

        throw new \LogicException('Unknown log instance');

    }

}