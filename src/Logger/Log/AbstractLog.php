<?php

/*
 * This file is a part of small-static-http
 * Copyright 2022-2023 - Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace SmallStaticHttp\Logger\Log;

use Small\Logger\Log\BasicLog;
use SmallStaticHttp\Kernel\Kernel;
use SmallStaticHttp\Logger\Enum\LogLevelType;

Abstract class AbstractLog extends BasicLog
{

    public function __construct(
        LogLevelType $level,
        string $message,
    ) {
        parent::__construct(new \DateTime(), $level->name, $message);
    }

    /**
     * Return kind of log string
     * @return string
     */
    abstract public function getKind(): string;

    public function jsonSerialize(): mixed
    {
        $array = parent::jsonSerialize();
        $array['kind'] = $this->getKind();
        if (Kernel::$container->hasParameter('static-log-fields')) {
            foreach (Kernel::$container->getParameter('static-log-fields') as $key => $value) {
                $array[$key] = $value;
            }
        }

        return $array;
    }

}