<?php

/*
 * This file is a part of small-static-http
 * Copyright 2022-2023- Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace SmallStaticHttp\Logger\Log;

use SmallStaticHttp\Logger\Enum\LogLevelType;
use Swoole\Http\Request;

class HttpAccessLog extends AbstractLog
{

    public function __construct(
        LogLevelType $level,
        string $message,
        protected Request $request,
    ) {
        parent::__construct($level, $message);
    }

    public function getKind(): string
    {
        return 'access';
    }

    public function jsonSerialize(): mixed
    {
        $array = parent::jsonSerialize();
        $array['method'] = $this->request->server['request_method'];
        $array['uri'] = $this->request->server['request_uri'];
        $array['ip'] = $this->request->server['remote_addr'];
        $array['user-agent'] = $this->request->header['user-agent'];
        $array['hostname'] = $this->request->header['host'];
        $array['protocol'] = $this->request->server['server_protocol'];

        return $array;
    }

}