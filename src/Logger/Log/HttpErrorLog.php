<?php

/*
 * This file is a part of small-static-http
 * Copyright 2022-2023- Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace SmallStaticHttp\Logger\Log;

use SmallStaticHttp\Logger\Enum\LogLevelType;
use Swoole\Http\Request;

class HttpErrorLog extends HttpAccessLog
{

    public function __construct(
        LogLevelType $level,
        string $message,
        protected Request $request,
        protected \Throwable|null $throwable = null,
    ) {
        parent::__construct($level, $message, $request);
    }

    public function getKind(): string
    {
        return 'error';
    }

    public function jsonSerialize(): mixed
    {
        $array = parent::jsonSerialize();
        if ($this->throwable != null) {
            $array['exception'] = get_class($this->throwable);
            $array['message'] = $this->throwable->getMessage();
            $array['file'] = $this->throwable->getFile();
            $array['line'] = $this->throwable->getLine();
            $array['trace'] = $this->throwable->getTraceAsString();
        }

        return $array;
    }

}