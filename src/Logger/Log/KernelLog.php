<?php

/**
 * This file is a part of small-static-http
 * Copyright 2022-2023 - Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace SmallStaticHttp\Logger\Log;

class KernelLog extends AbstractLog
{

    /**
     * Return kind of log string
     * @return string
     */
    public function getKind(): string
    {
        return 'kernel';
    }

}