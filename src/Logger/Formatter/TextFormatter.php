<?php

/*
 * This file is a part of small-static-http
 * Copyright 2022-2023 - Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace SmallStaticHttp\Logger\Formatter;

use Small\Logger\Contracts\FormatterInterface;
use Small\Logger\Contracts\LogInterface;
use SmallStaticHttp\Logger\Log\AbstractLog;

class TextFormatter implements FormatterInterface
{

    public function format(LogInterface $log): mixed
    {
        if (!$log instanceof AbstractLog) {
            throw new \LogicException('Not a small-static-http-log');
        }

        return '[' . $log->getKind() . '] ' .
            ' - ' . $log->getDateTime()->format('H:i:s H:i:s.u') . ' - ' .
            ' [' . $log->getLevel() . '] - ' .
            $log->getMessage();
    }

}